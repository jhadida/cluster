
> **Under construction!** (Jan 2019)

[![Documentation](https://img.shields.io/badge/Documentation-https%3A%2F%2Fjhadida.gitlab.io%2Fcluster-orange.svg)](https://jhadida.gitlab.io/cluster)

# Cluster utilities

This repository is a collection of useful scripts & gotchas to work with clusters and containers in Oxford. Hosted on [GitLab](https://jhadida.gitlab.io/cluster/).
