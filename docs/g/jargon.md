
# Cluster jargon

## Cluster, Node, Queue

Cluster is a set of nodes. 
Compute node is a resource capable of executing jobs.

## Job, Task

A task is a unit of work to be executed, with its own ID, return status, logs, and resource constraints (number of cores, RAM, etc.).

A job consists of one or several tasks, and a contract of resource usage (e.g. maximum compute duration, maximum memory usage, etc.).

## Status, Logs

Managing the assignment and execution of jobs on a cluster of dozens of computers requires some orchestration.
This orchestration is handled by a computer program; at FMRIB this program is called the [**grid engine**](https://en.wikipedia.org/wiki/Oracle_Grid_Engine), but for the sake of generality we shall call it the **supervisor**.

The supervisor needs to be made aware of the various compute-node available and their properties (e.g. number of CPUs, RAM, etc.), and there is usually a large infrastructure of computer programs built around the supervisor. For example, the queues hold jobs, and the supervisor can poll and extract jobs from them. But also submission and monitoring. 

When a job executes, outputs (standard and error) captured to log file. Useful for diagnosing failures (e.g. bug or unexpected incident). Good practice to be paranoid in the code; always test inputs and intermediary states, e.g. empty array or complex or infinite values.

Job status can be waiting to execute, or for other job. It can be suspended, completed or failed. More on the [wiki](https://internal.fmrib.ox.ac.uk/i-wiki/Computing/Cluster/#Interpreting_qstat.27s_output).

## Parallel, Sequential

Running multiple tasks can either be done:
 - [[in sequence | jalapeno/sequence]] (one after the other), typically is one task requires the results of the previous task.
 - [[in parallel | jalapeno/parallel]] (random subsets executing at the same time), when the different tasks are independent.
