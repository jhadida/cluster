
# Bash basics

## Boilerplate

```bash
#!/bin/bash

# ------------------------------------------------------------------------
# Preamble

# error display in red
echoerr() { >&2 echo -e "$(tput setaf 1)$* $(tput sgr0)"; exit 1; }

# helptext
usage() {
cat <<EOF 
Very helpful helptext.

    Author: John Smith
EOF
exit 0;
}

# ------------------------------------------------------------------------
# Main

# extract arguments
(( $# < 1 )) && usage 
Argument=$1

# do stuff
```

---

## Execute

There are many ways to execute a bash script:

 - from an interactive shell: `./myscript.sh`
 - in the background: `./myscript.sh &`
 - by calling bash explicitly: `bash myscript.sh`
 - by sourcing the script from another one `source myscript.sh`
 - detached from the current shell: `nohup bash myscript.sh 2>&1 &`

## Tips

You should know about: `cd, ls, cp, mv, rm, ln`

 - `chmod, chown, chgrp`
 - `grep` 
 - `awk`
 - `find` (GNU)
 - `rsync`

## Resources

- [Manual](https://devdocs.io/bash/)
- [Cheatsheet](https://devhints.io/bash)
- [Unix SE](https://unix.stackexchange.com/)
- [Bash hackers](https://wiki.bash-hackers.org/)
