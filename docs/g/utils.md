
# Utilities

## Using nohup

```
nohup bash myscript.sh 2>&1 &

nohup bash myscript.sh 2>|log.err 1>|log.out &
```

## Slow filesystem

```
>> dd if=/dev/zero of=test-speed bs=10M count=1 oflag=direct
1+0 records in
1+0 records out
10485760 bytes (10 MB) copied, 0.700203 s, 15.0 MB/s

>> dd if=/dev/zero of=test-access bs=512 count=100 oflag=direct
100+0 records in
100+0 records out
51200 bytes (51 kB) copied, 28.5122 s, 1.8 kB/s
```
