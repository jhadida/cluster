
# Task management

## Bash scripts

Write a Bash script, optionally taking parameters, to run a task. See [how](bash).

## Executing a task

Different ways of executing a task:
 - Interactively on `jalapeno00` (or other dedicated node)
 - In the background on `jalapeno00` (or other dedicated node)
 - Submitting to a queue

## Configuration

This script defines a few variables that are used in other scripts.
It is intentionally not made executable (`chmod 644`), because it shouldn't
be called as a script; instead, it should be sourced by other scripts.

**Remember:** in bash THERE IS NO SPACE AROUND THE `=` in an assignment:
```
EMAIL=jhadida@fmrib.ox.ac.uk           OK
EMAIL = jhadida@fmrib.ox.ac.uk         NOT OK
```

## Generators

Task generators are Bash scripts which print a list of tasks in the terminal.
This output can be streamed to a file, and submitted as an "array" of jobs, to be run in parallel on the cluster.

## Archiving

It will take a lot of practice for you to write task that never need to be re-run:
 - either because it takes experience to predict all the things that could potentially go wrong during execution;
 - or simply due to the nature of doing research: for example, you might need to compute an additional piece of information during subject-wise analysis, because of some refinement that appeared later on in your group-wise pipeline.
