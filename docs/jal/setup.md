
# Machines

The Jalapeno cluster is hosted at FMRIB, and managed by SGE `8.1.9`. 

See also [FMRIB's help page](https://internal.fmrib.ox.ac.uk/i-wiki/Computing/Cluster/) (requires login).

## SSH host configuration

Access to the machines below is easier with a bit of configuration.

Edit the file `~/.ssh/config` (create it if it does not exist), and make sure it contains the following:

```
Host remote-jalapeno
    HostName jalapeno.fmrib.ox.ac.uk
    User {username}
    ForwardAgent yes

Host remote-jalapeno00
    User {username}
    ForwardAgent yes
    ProxyCommand ssh -q remote-jalapeno -W jalapeno00:22
```

where you should replace `{username}` with your username (e.g. `jhadida`), and you can add `IdentityFile "~/.ssh/mykey"` directives if you use a non-default key for this connection.

You can then simply type from your terminal e.g.:
```
ssh [-Y] remote-jalapeno00
```
to login to `jalapeno00` from outside FMRIB.


## Jalapeno

```
jalapeno.fmrib.ox.ac.uk: gateway (not for compute)

Connect within FMRIB:
    ssh [-Y] {username}@jalapeno

Connect from outside:
    ssh [-Y] {username}@jalapeno.fmrib.ox.ac.uk
```

**DO NOT RUN COMPUTATIONS HERE.** 

This is a [gateway](https://en.wikipedia.org/wiki/Gateway_(telecommunications)) for users to connect from outside FMRIB. Once connected to `jalapeno`, you can submit jobs using `fsl_sub`, or connect to `jalapeno00` to run small computations (typically for exploration/development). 

## Jalapeno00

```
jalapeno00: scratch compute

Connect within FMRIB:
    ssh [-Y] {username}@jalapeno00

Connect from outside: (see host configuration above)
```

This is a _scratch-server_ intended:
 - to run reasonably small tasks (see the _Rule of Twos_); 
 - to develop bigger ones incrementally (e.g. prepare scripts to be submitted);
 - to run exploratory analysis on small datasets;
 - to prepare and submit jobs with `fsl_sub`;
 - to manage your files. 

Basically any "quick & dirty" task or data-management can be run here. A simple way to know whether or not something should be submitted is as follows:

> **Rule of Twos:**
> If it requires >16 GB of RAM, or >8h to run with >4 CPUs, it should be submitted.
 
It can only be accessed directly from within FMRIB, but you can tunnel-through `jalapeno` from the outside.

## Other machines

 - `cuda03`: restricted access
 - `jalapeno18`: for jobs consuming a LOT of RAM
