
# Job submission

Good practices to submit jobs with `fsl_sub`.

---

## Single job

Good practice:

 - [[ Execute script | jalapeno/execute ]]
 - Submission script
 - Logs and lock
 - [[ Archiving | jalapeno/archive ]]

---

## Parallel jobs

Same as [[single | jalapeno/single]], but with [[ generator scripts | jalapeno/generator ]] and task files.

Monitoring and [[ archiving | jalapeno/archive ]] become slightly more complicated.

---

## Sequential jobs

Sequential jobs typically consist of a series of tasks, each of which depends on results computed previously.

The mechanism to ensure sequence is an event-based FIFO queue.

There are very few cases where cluster-enforced sequentiality cannot be replaced with manually imposed sequentiality e.g. in the form of a wrapper script:
 - In a script, the code is executed from top to bottom, and the execution is cancelled if an error occurs.
 - Sequentiality is particularly useful in a map/reduce context, where many tasks are executed in parallel as a first step, and a summary step is then executed to consolidate results.
