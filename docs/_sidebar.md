
* General

  * [Bash](g/bash)
  * [Utils](g/utils)
  * [Jargon](g/jargon)
  * [Singularity](g/singularity)

* Jalapeno

  * [Setup](jal/setup)
  * [Task](jal/task)
  * [Submit](jal/submit)

* Arcus

  * [Setup](arc/setup)
  * [Jargon](arc/jargon)
  * [SLURM](arc/slurm)
  * [Tools](arc/tools)
