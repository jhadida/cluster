
# Setup

Hosted by the [ARC](http://www.arc.ox.ac.uk/) in Oxford.

## Servers

```
On VPN:
    oscgate.arc.ox.ac.uk
    myaccount.arc.ox.ac.uk

Internal compute servers:
    arcus-b
    arcus-htc
```

To connect to one of the compute servers:
```
ssh   <USER>@oscgate.arc.ox.ac.uk   -W arcus-htc:22 
```

## Account 

To manage account, connect to `myaccount.arc.ox.ac.uk` and:

 - `foo` gives the storage quota
 - `bar` gives the compute quota
 - `baz` resets the password

## Folders



## Modules


