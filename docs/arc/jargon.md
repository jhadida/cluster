
# Jargon

> More info on the [official website](https://slurm.schedmd.com/).
> See also the [Rosetta Stone](https://slurm.schedmd.com/rosetta.html) of workload managers. <br/>
> **Terms:** queue, partition, allocation, job, task, node, cpu, core


## Node, core

- A node is an abstraction of a computing resource (i.e. capable of executing code), characterised by a set of features (e.g. number of CPUs, amount of RAM, etc.).
- Several nodes may actually be hosted on the same physical machine, although the sharing of resources is mutually exclusive between nodes. 
- Each node can have a certain amount of RAM and multiple GPUs/CPUs of its own; in the context of workload management, CPUs and cores are the same thing.

## Task

- A task is a unit of computational work to be executed on a single node. 
- Tasks are typically written as standalone scripts, which may/not expect inputs.
- Multiple tasks can be executed on the same node, or even on the same CPU.
- The standard output+error of a task are typically streamed to log-files.

## Job

- A job is a set of tasks, to be executed following a set of directives (e.g. in parallel, with job name X, etc.), and subject to a set of constraints (e.g. number of CPU per node, maximum runtime, etc.). 
- A job can require multiple nodes to execute its tasks (typically one node per task), with constraints applying to each and every node.

## Partition

- A partition is a subset of nodes, on which the workload manager can perform allocations of resources to execute tasks.
- When a job is submitted to the workload manager, it sits in a queue waiting for an allocation of resources which satisfies the specified constraints.