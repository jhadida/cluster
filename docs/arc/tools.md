
# Tools

## Recipe

- Write a script that may take inputs
- Write a task file in which each line is a call to the previous script to be executed
- Submit the task file using either `jsub_array` or `jsub_multi`
- Use `jsub_info` to check the status of individual tasks

## Parallel jobs

Either as a job-array (with built-in policies for concurrency):
```
jsub_array tasks.job logs/ 100 \
    --job-name=foo --time=300 --cpus-per-task=2 --mem=16GB
```
where `100` specifies that no more than 100 tasks are to be run concurrently (set to 0 for unlimited).

Or as multiple jobs (less admin-friendly):
```
jsub_multi tasks.job logs/ \
    --job-name=foo --time=300 --cpus-per-task=2 --mem=16GB
```
