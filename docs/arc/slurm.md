
# Submitting jobs with SLURM

```
sbatch [options] script [args...]

Options:

--array=<indexes>
        Submit multitple jobs to be executed in parallel with identical parameters.
        Indices can be:
            comma-separated list        1,2,134,308
            dash-separated range        1-10
            range with step             1-10:2

        Limit number of concurrent jobs allowed with suffix: %N 
        For example: 1-10,42%4  (4 concurrent max)

--workdir
        Move there before starting the batch script.
--input
        Stream file to standard input. Filename can contain:
            %%     The character "%".
            %A     Job array’s master job allocation number.
            %a     Job array ID (index) number.
            %j     Job allocation number.
            %N     Node name.  Only one file is created, so %N will be replaced by the name of the first node in the job, which is the one that runs the script.
            %u     User name.
--output
        Stream standard output to file.
--error
        Stream standard error to file.
--open-mode=append|truncate
        Change the mode of file-opening for the logs.

--job-name
        Name to display in queue.



--begin=<time>
        Submit job at specified time:
            exact time          HH:MM:SS
            exact date          YYYY-MM-DD[THH:MM[:SS]]     (eg: 2010-01-20T12:34:00)
            relative time       now+1hour                   (keywords: now / today / tomorrow)
            relative date       today+3days                 (units: seconds, minutes, hours, days, weeks)
            time-of-day         midnight / noon / teatime (4pm)

--deadline=<opt>
        Do not start the job, if no ending is possible by that deadline. Formats:
            HH:MM
            YYYY-MM-DD[THH:MM]

--time
        Maximum amount of time allowed for the entire job to complete. Formats:
            MM
            DD-HH

--hold
        Submit job in a help state. Can be released with:   
            scontrol release <jobid>

--dependency=<dependency_list>
        Depend on other jobs:
            after:job_id[:jobid...]
            afterany:job_id[:jobid...]
            afterok:job_id[:jobid...]
            afternotok:job_id[:jobid...]
            aftercorr:job_id[:jobid...] 
                (A task of job-array can be submitted as soon as a task from the other job has completed successfully.)




--ntasks
        Number of tasks to be run within the job.
--nodes=<minnodes[-maxnodes]>
        Number of nodes required to run the job. Default to 1 task/node.
--ntasks-per-node
--ntasks-per-core
        Default to 1 task/node.
--mincpus=<n>
        Minimum number of (virtual) CPU per node.
--cpus-per-task
        Number of cpus required on a single node for each of the tasks in the current job. Default is 1.
--mem   
--mem-per-cpu
```
