#!/bin/bash
#
# Submit task file as a job array, with one task per line.

# utils
ColR=$(tput setaf 1)
FontD=$(tput sgr0)

echoerr() { >&2 echo -e "${ColR}$*${FontD}"; exit 1; }

# check that command used to execute task exists
TaskExec=jsub_run
[ -n "$(which "$TaskExec" 2>/dev/null)" ] || echoerr "Command not found: $TaskExec"

# ------------------------------------------------------------------------

usage() {
cat <<EOF 
Submit task file as job array.

    jsub_array <TaskFile> <LogDir> <MaxConcurrent> <Args...>


EXAMPLE:

Submit task file 'foo.job' with no more than 50 concurrent tasks, with
16GB memory and 2 CPUs for each task, and save outputs in folder foo_log:

    jsub_array foo.job foo_log 50 \
        --mem=16GB --cpus-per-task=2

INPUTS:

    TaskFile
        File with one task to be executed per line.

    LogDir
        Folder in which standard and error outputs will be saved.

    MaxConcurrent
        Maximum number of concurrent tasks.
        Set to 0 to allow any.

    Args...
        Additional options to be forwarded to the sbatch command.


NOTES:

    The LogDir folder will be created if it does not exist.
    This will fail if multiple nested folders need to be created.

    Stdout is redirected to the log file: J%A_T%a.out
    Stderr is redirected to the log file: J%A_T%a.err

EOF
exit 0;
}

[ $# -lt 3 ] && usage

TaskFile=$1
LogDir=${2%/}
MaxCon=$3
shift 3

# ------------------------------------------------------------------------

# check inputs
[ -f "$TaskFile" ] || echoerr "Not found: $TaskFile"
[ -d "$LogDir" ] || mkdir "$LogDir" || echoerr "Could not create: $LogDir"
[ -w "$LogDir" ] || echoerr "Cannot write to folder: $LogDir"
(( $MaxCon >= 0 )) || echoerr "MaxCon should be non-negative."

# build command
Ntask=$(wc -l "$TaskFile" | awk '{ print $1 }')
(( $Ntask >= 1 )) || echoerr "Empty task file: $TaskFile"

if (( $MaxCon > 0 )); then
    arr="1-${Ntask}%$MaxCon"
else 
    arr="1-${Ntask}"
fi

# submit job
LogPrefix="$LogDir/J%A_T%a"
sbatch --array=$arr \
    --output="${LogPrefix}.out" --error="${LogPrefix}.err" "$@" \
    "$TaskExec" "$TaskFile"

