#!/bin/bash
#
# Information about jobs submitted with jsub_array or jsub_multi.

# utils
ColR=$(tput setaf 1)
ColG=$(tput setaf 2)
ColB=$(tput setaf 4)
FontD=$(tput sgr0)

echoR() { echo -e "${ColR}$*${FontD}"; }
echoG() { echo -e "${ColG}$*${FontD}"; }
echoB() { echo -e "${ColB}$*${FontD}"; }
echoerr() { >&2 echo -e "${ColR}$*${FontD}"; exit 1; }

# ------------------------------------------------------------------------

usage() {
cat <<EOF 
Information about job submitted with jsub_array or jsub_multi.

    jsub_info <Command> <TaskFile> <LogFile> <Args...>

INPUTS:

    Command
        One of the commands listed below.

    TaskFile
        The task file used for job submission.

    LogFile
        Any of the log-files created for the particular submission 
        of interest. Three things are determined from this:
            1. The folder in which the logs are being saved
            2. The type of submission that was used (_array or _multi)
            3. The prefix of the corresponding files

    Args...
        Additional options depending on the command.


COMMANDS:

    lspending
        List tasks that have not run yet.

    lsfail <Pattern>
        List tasks for which the .err file is not empty.
        If a pattern is specified, then failure depends on finding that pattern
        in the error file instead.

    task <TaskID>
        Show information about a particular task.


NOTES:


EOF
exit 0;
}

[ $# -lt 3 ] && usage

Command=$1
TaskFile=$2
LogFile=$3
shift 3

[ -f "$TaskFile" ] || echoerr "Task file not found: $TaskFile"

# ------------------------------------------------------------------------

# read task ID from log-name
extract_taskid() {
    echo "$1" | perl -ne '/_T(\d+).*/ && print $1'
}

# reconstruct filename of log
task_logname() {
    echo "$LogBase" | perl -pe "s/_T(\d+)/_T$1/g"
}

# show info about task
task_info() {
    
    TaskID=$1
    [ -n "$TaskID" ] || echoerr "No task ID."
    
    TaskLine=$(sed "${TaskID}q;d" "$TaskFile")
    [ -n "$TaskLine" ] || echoerr "Empty task"
    
    LogName=$(task_logname $TaskID)
    TaskErr="$LogDir/${LogName}.err"
    
    if [ -s "$TaskErr" ]; then 
        echoR "------------------------------ #$TaskID"
    elif [ -f "$TaskErr" ]; then 
        echoG "------------------------------ #$TaskID"
    else 
        echoB "------------------------------ #$TaskID"
    fi

    echo -e "Logname:  $(task_logname $TaskID)"
    echo -e "Command:  ${TaskLine}\n"

    echo "Info:"
    jobinfo "$TaskID"
}

# ------------------------------------------------------------------------

# determine log folder
LogDir=$(dirname "$LogFile")
LogBase=$(basename "$LogFile")
LogBase=${LogBase%.*}

# determine submission type
if [[ $LogBase =~ ^J.* ]]; then 
    SubType=array
elif [[ $LogBase =~ ^P.* ]]; then 
    SubType=multi
else 
    echoerr "Submission type not recognised: $LogBase"
fi

# main switch
case $Command in
lspending|lsp)
    NTask=$(wc -l "$TaskFile" | awk '{ print $1 }')
    for tid in $(seq $NTask); do 
        LogName=$(task_logname "$tid")
        [ -f "$LogDir/${LogName}.out" ] || echo "$tid"
    done
    ;;
lsfail|lsf)
    if [ $# -gt 0 ]; then 
        Pat="$1"
        shift 
        Files=($(grep --color=never --include="*.err" "$@" -Elr "$LogDir" -e "$Pat"))
    else 
        Files=($(find "$LogDir" -type f -name '*.err' -size +0))
    fi
    for f in "${Files[@]}"; do 
        echo $(extract_taskid "$f")
    done
    ;;
task|t)
    for tid in "$@"; do 
        task_info $tid
    done
    ;;
*)
    echoerr "Unknown command: $Command"
    ;;
esac
