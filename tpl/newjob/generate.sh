#!/bin/bash

# -------------------------------------
# 1. Parse inputs (optional)
# -------------------------------------

# 1.1 Check number of inputs
if (( $# < 1 )); then 
    >&2 echo "Usage: $0 <Param>"
    exit 1
fi

# 1.2 Give a name to inputs to avoid confusions
Param=$1


# -------------------------------------------
# 2. Iterate over parameters and print tasks
# -------------------------------------------

for subjectID in $(seq -f '%02g' 1 10); do
    echo "./exec-matlab.sh $subjectID"
done

