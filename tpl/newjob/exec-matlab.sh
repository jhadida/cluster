#!/bin/bash
#
# This script calls Matlab with a given set of parameters, and executes a single job.
#
# You should check that all arguments are provided, define your Matlab command with
# these arguments, and specify the folders where your Matlab scripts live. Everything
# is explained below.


# --------------------------
# 1. Extract parameters
# --------------------------

# 1.1 Check that there are enough arguments
if (( $# < 3 )); then # $# is the number of arguments
    echo "Usage: $0 <NDim> <Method> <ID>"
    exit 1
fi

# 1.2 Give them a name to avoid confusion
NDim=$1
Method=$2
Id=$3


# --------------------------
# 2. Execute the job
# --------------------------

# 2.1 Where is the root folder? (on the cluster!)
#
# This is where your startup.m is.
# If there is nothing to setup before running the job itself, then comment this line.
#
STARTDIR="/home/fs0/jhadida/Documents/work"

# 2.2 Where is the working folder? (on the cluster!)
#
# This is where the function to call lives.
#
CALLDIR="/home/fs0/jhadida/Documents/work/app/meg"


# 2.3 What is the command you would like to run?
#
# This is the command as you would type it in the Matlab console, assuming you were in
# the working folder to start with.
#
COMMAND="wip.smcCompare.thesis${NDim}( '${Method}', ${Id} );"


# 2.4 Run that command!
if [ -n "$STARTDIR" ]; then
    matlab -nodisplay -nodesktop -singleCompThread -r "cd '${STARTDIR}'; startup; cd '${CALLDIR}'; ${COMMAND}; quit;"
else
    matlab -nodisplay -nodesktop -singleCompThread -r "cd '${CALLDIR}'; ${COMMAND}; quit;"
fi

