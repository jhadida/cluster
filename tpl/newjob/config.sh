
# Your e-mail is used to notify you of unexpected things that happened to your jobs
EMAIL=username@fmrib.ox.ac.uk

# The project name is used to identify your tasks (qstat will show that name)
PROJECT=SpecialUnicorn

# The default queue is used by submit.sh
DEFAULT_QUEUE=long.q

#---------------------------------------------------------------------------
# Don't touch anything below

CURRDIR=$(pwd)
THISDIR=$(dirname "${BASH_SOURCE}")
