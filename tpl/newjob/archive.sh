#!/bin/bash
#
# This script takes an optional folder name (default _arc), and 

set -e

# deal with inputs
(( $# < 2 )) && { echo "Usage: $0 <Name> <Folder: _archive>"; exit 1; }

Name=${1%.*}
Folder=${2:-_archive}
TimeStamp=$(date +"%y-%m-%d_%H%M%S")

# check that job, task and/or log folders exist
[ -f "${Name}.task" ] && TaskFile=${Name}.task 
[ -f "${Name}.job" ] && JobFile=${Name}.job 
[ -d "${Name}-logs" ] && LogFolder=${Name}-logs

# create archive
Folder=${Folder%/} # no trailing slash
[ ! -d "$Folder" ] && mkdir -pv "$Folder"
tar cvf "${Folder}/${TimeStamp}.tar" "$TaskFile" "$JobFile" "$LogFolder"

# remove archived contents
# [ -n "$TaskFile" ] && rm -f "$TaskFile"
# [ -n "$JobFile" ] && rm -f "$JobFile"
# [ -n "$LogFolder" ] && rm -rf "$LogFolder"
