
## Generators

This script prints a list of calls to the function exec.matlab to the console, with 
different parameters corresponding to the jobs we would like to compute.

Such a list is typically generated with one or several nested for-loops, iterating
over the parameters of interest. 

In most cases, this generating script will not take any input, but it is conceivable
that more complicated tasks might require a parametric generator.
For the sake of clarity, try to keep things simple; it is better (and quicker) to 
write a few generators which list slightly different tasks, than to write a complex
generator which can list wildly different tasks. It's all about being able to identify
the source of a task file: if it requires some thinking to figure out which parameters 
were given to the generator in order to generate such existing task file, you are just 
making things difficult to trace back down the line.


**Example:**

We have a Matlab script which, given a subject ID, runs a resting-state analysis on 
some pre-processed fMRI or M|EEG data. Say that we have a dataset of N subjects, and 
we would like to run the analysis on each of them. 

The first thing to do is to edit the file exec.matlab, such that it takes a single 
input (the subject ID), and calls our Matlab script with that ID. Make sure you set
all the paths correctly, and make a few test calls to check that it works.

Then, the second thing to do is to edit this bash script, to iterate over the N
subject IDs, and print a call to exec.matlab for each ID:

```
for subjectID in {1..N}; do
    echo "./exec.matlab $subjectID"
done
```
  
We can then call this bash script and stream its output to a task file:
```
./generate-task.sh > task
```

The main advantage of doing this is that if the list changes for some reason, or
the processing fails for some subjects and needs to be re-run, or we would like to
include another parameter, or process another dataset with the same exec.matlab, we
can simply copy and modify generate-task.sh in order to create a new task file.

It's a good idea to give matching names to the task files and the scripts that 
generate them, e.g. `generate-mySuperTask.sh > mySuperTask`


## Matlab task

Use the named parameters that you extracted in section 1.2 in order to put together
the command that you want to run, with the right parameters.

If you have several commands to run, it is best (for clarity) to put them together 
in a separate Matlab function, and call that function here.


> QUOTE STRINGS CORRECTLY !!
> DONT QUOTE NUMBERS       (unless your script expects strings)

For example if you have a parameter `Foo=$1` defined above, you can quote it like this:
```
COMMAND="myfunction( '${Foo}' )"   
    (see the single quotes inside the double quotes?)
```   


> !! TEST THIS COMMAND IN MATLAB BEFORE SENDING IT TO THE CLUSTER !!

 - At the beginning of your function, validate your parameters (e.g. x is a string, 
   y is a valid filename, z is a positive number, etc.).

 - Monitor how much memory your script requires; if you take more than 8GB, your
   jobs will be killed automatically. Check out the queue `bigmem.q`

 - Monitor how much time your script requires; to be conservative, multiply by 2
   the time it takes on your laptop, and choose the appropriate cluster queue.
   NOTE: it WILL take longer to execute on the cluster!

 - Try making your job reasonably efficient: you'll get your results faster, and more 
   people will be able to compute their stuff too. Use the Matlab profiler.

For more details about profiling / memory, see: 
  https://uk.mathworks.com/help/matlab/performance-and-memory.html

Finally, remember it will be running on the cluster, not on your laptop!
A good way to make sure everything is fine is to ssh on jalapeno00 (not jalapeno!), 
start a Matlab session (-nodisplay, -nodesktop), and run your command there.

