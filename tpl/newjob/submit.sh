#!/bin/bash
# 
# Job-file can contain the following info:
#
#   Name            Job name (used for log-folder)
#   Task            Task file to submit
#   Cmd             Command to execute (override Task)
#   Queue           Submission queue (default from config)
#   Threads         Number of threads (default 1)
#

echoerrx1() { >&2 echo "$*"; exit 1; }

# Load configuration
source ./config.sh

# one input required
(( $# < 1 )) && echoerrx1 "Usage: $0 <JobFile|TaskFile>"

# make sure file exists
[ ! -f "$1" ] && echoerrx1 "File not found: $1"

# depending on file extension
Name="${1%.*}"
Ext="${1##*.}"
case "$Ext" in 
job)
    # source job file, and set properties
    source "$1"

    # make sure everything is set
    Queue=${Queue:-$DEFAULT_QUEUE}      # default from config
    Threads=${Threads:-1}               # default 1

    # if no command is set, then look for task file
    if [ -z "$Cmd" ]; then 
        Task=${Task:-"${Name}.task"}    # default from filename
        if [ -f "$Task" ]; then 
            Cmd="-t '${THISDIR}/$Task'"          # set command
        else 
            echoerrx1 "Task-file not found: $Task"
        fi
    fi
    ;;

task)
    # if job-file doesn't exist, create one
    Job="${Name}.job"
    touch "$Job"
    
    # running with job-file
    echo "Running with job-file: $Job"
    ./submit.sh "$Job"
    exit 0
    ;;

*)
    echoerrx1 "Missing or unrecognised file extension: '$Ext'"
esac

# confirm execution
echo ""
echo "Confirm job submission:"
echo "     Name: $Name"
echo "  Command: $Cmd"
echo "    Queue: $Queue"
echo "  Threads: $Threads"
echo ""
read -r -p 'Continue? [y/N] ' response
case "$response" in
[yY][eE][sS]|[yY]) 
    # keep going
    ;;
*)
    echoerrx1 'Cancelled'
esac

# create log-folder
LogFolder=${Name}-logs
[ ! -d "$LogFolder" ] && mkdir -v "$LogFolder"
[ ! -w "$LogFolder" ] && echoerrx1 "Permission error. Cannot write to folder: $LogFolder."
echo "Logs will be saved in: $LogFolder"

# Make sure the job isn't currently running
LockFile=${LogFolder}/lock
if [ -f "$LockFile" ]; then 
    echo ""
    echo "Uh oh.. It looks like this job might already be running."
    echo ""
    echo "Type qstat and check for ID $(cat "$LockFile")."
    echo "If it's not there, then something went wrong; just remove"
    echo "the file '$LockFile' and try submitting again."
    exit 0;
fi

# Otherwise submit
if [ $Threads -gt 1 ]; then
    TaskID=$(fsl_sub -q "$Queue" -M "$USER" -m a -N "$PROJECT" -l "$LogFolder" -s openmp,$Threads $Cmd)
else
    TaskID=$(fsl_sub -q "$Queue" -M "$USER" -m a -N "$PROJECT" -l "$LogFolder" $Cmd)
fi

# create a lock, and register its deletion
echo "$TaskID" >| "$LockFile"
LockID=$(fsl_sub -j "$TaskID" -q veryshort.q -N "${PROJECT}-lock" -l "$LogFolder" rm -f "$LockFile")

# backup configuration in log folder
cat <<EOF > "${LogFolder}/${TaskID}.job"
Name="$Name"
Cmd="$Cmd"
Queue=$Queue
Threads=$Threads
TaskID=$TaskID
LockID=$LockID
EOF
