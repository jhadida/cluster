#!/bin/bash
# 
# This script executes a particular task from a task-file on the current machine.

echoerrx1() { >&2 echo "$*"; exit 1; }

# two inputs required
(( $# < 2 )) && echoerrx1 "Usage: $0 <TaskFile> <LineNumber>"

# extract params
File=$1
LineNum=$2
Line=$(sed "${LineNum}q;d" "$File")

# make sure file and line exist
[ ! -f "$File" ] && echoerrx1 "File not found: $File"
[ -z "$Line" ] && echoerrx1 "Line '${LineNum}' either doesn't exist or is empty."

# confirm execution
echo ""
echo "Confirm execution in the background of:"
echo "  $Line"
echo ""
read -r -p 'Continue? [y/N] ' response
case "$response" in
[yY][eE][sS]|[yY]) 
    # keep going
    ;;
*)
    echoerrx1 'Cancelled'
esac

# run command
LogFolder=${File%.*}-logs
run-nohup "$LogFolder" "$Line"
